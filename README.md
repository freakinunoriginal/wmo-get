# wmo-get

Bash script to get a day's forecast data from the World Meteorological Organization (wmo.int), outputs as City, Date, Weather, Low, and High. Can take arguments for city ID and days beyond tomorrow.

### Requirements

curl, jq

### Data Source

See https://worldweather.wmo.int/en/dataguide.html

## Usage

Run the script to get tomorrow's forecast for the city ID defined in the script, or pass arguments.

* First argument for city ID, from https://worldweather.wmo.int/en/json/full_city_list.txt
* Second argument for days beyond tomorrow; WMO JSON data is forecast (not current) so array item 0 is tomorrow


### Example

`wmo-get.sh 183 1`

Will get the forecast for city ID 183 (Tokyo) for two days from now

```
{
  "City": "Tokyo",
  "Date": "2019-01-24",
  "Weather": "Partly Cloudy",
  "Low": "1",
  "High": "10"
}
```

## Apologies

My style is horrible; but it fits on 10 lines!