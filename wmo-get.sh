#!/bin/bash
# Bash script for reading from World Meteorological Organization JSON data
# Scott Bay, 22 January 2019, no rights reserved
# Requires: curl, jq; Arguments: $1=city, $2=days beyond tomorrow
city=185; # set your default city ID here from https://worldweather.wmo.int/en/json/full_city_list.txt
if [ "$1" != "" ] ; then city=$1; fi; # argument 1, city
day=0; if [ "$2" != "" ] ; then day=$2; fi; # argument 2, days beyond tomorrow
wmourl="http://worldweather.wmo.int/en/json/"; wmourl+=$city; wmourl+="_en.xml"; # make the city URL
# and now get the forecast for that city; change to minTempF/maxTempF for Fahrenheit
curl $wmourl --silent | jq --argjson day "$day" '{City: .city.cityName, Date: .city.forecast.forecastDay[$day].forecastDate, Weather: .city.forecast.forecastDay[$day].weather, Low: .city.forecast.forecastDay[$day].minTemp, High: .city.forecast.forecastDay[$day].maxTemp}';
